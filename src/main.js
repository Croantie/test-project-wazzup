import Vue from 'vue'
import App from './App.vue'
import store from './store'
import axios from 'axios'

const base = axios.create({
  baseURL: ' http://www.filltext.com/?rows=1000&id={index}&fullname={firstName}~{lastName}&company={business}&email={email}&uname={username}&address={addressObject}'
});

Vue.prototype.$http = base;
Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App)
}).$mount('#app');
