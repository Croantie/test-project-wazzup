import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        users: [],
        headers: [],
        loading: false,
        pagination: {
            page: 1,
            total: 0,
            rowsPerPage: 100
        },
        sortType: null,
        sortDirection: 'up',
        showModal: false,
        selectedUser: null,
        searchQuery: '',
        searchList: [],
        noUsers: false,
        error: ''
    },
    mutations: {
        ADD_USERS(state, payload) {
            state.users = payload
        },
        ADD_HEADERS(state, payload) {
            state.headers.push(payload);
        },
        DELETE_ID_FROM_HEADERS(state) {
            state.headers.shift()
        },
        CHANGE_LOADING_STATUS(state) {
            state.loading = !state.loading
        },
        SET_TOTAL_ITEMS(state) {
            state.pagination.total = state.searchList.length ? state.searchList.length : state.users.length
        },
        NEXT_PAGE(state) {
            state.pagination.page++
        },
        PREV_PAGE(state) {
            state.pagination.page--
        },
        FIRST_PAGE(state) {
            state.pagination.page = 1
        },
        LAST_PAGE(state, payload) {
            state.pagination.page = payload
        },
        PARTICULAR_PAGE(state, payload) {
            state.pagination.page = payload
        },
        SHOW_MODAL(state) {
            state.showModal = !state.showModal
        },
        GET_USER_INFO(state, payload) {
            state.selectedUser = payload
        },
        CHANGE_DIRECTION(state) {
            state.sortDirection = state.sortDirection === 'up' ? 'down' : 'up'
        },
        CHANGE_SORT_TYPE(state, payload) {
            state.sortType = payload;
        },
        RESET_DIRECTION(state) {
            state.sortDirection = 'up'
        },
        SORT_USERS(state, payload) {
            const type = payload;
            let newUsers;
            type === 'address' ?
                state.sortDirection === 'up' ? newUsers = state.users.sort((a, b) => a[type].state.localeCompare(b[type].state)) :
                    newUsers = state.users.sort((a, b) => b[type].state.localeCompare(a[type].state))
                :
                state.sortDirection === 'up' ? newUsers = state.users.sort((a, b) => a[type].localeCompare(b[type])) :
                    newUsers = state.users.sort((a, b) => b[type].localeCompare(a[type]));
            state.users = newUsers;
        },
        SEARCH_USERS(state) {
            state.searchList = [];
            let newUsers;
            newUsers = state.users.filter(items => {
                for (let item in items) {
                    if(String(items[item]).toLowerCase().indexOf(state.searchQuery.toLowerCase()) !== -1) {
                        return true
                    }
                }
                return false
            });
            state.searchList = newUsers
        },
        NO_SEARCH_RESULTS(state) {
            state.noUsers = !state.noUsers
        },
        HANDLE_ERROR(state, payload) {
            state.error = payload
        }
    },
    actions: {
        getUsers({commit, dispatch, getters}) {
            commit('CHANGE_LOADING_STATUS');
            this._vm.$http.get('')
                .then(async response => {
                    await commit('ADD_USERS', response.data);
                    commit('CHANGE_LOADING_STATUS');
                })
                .then(() => {
                    if (getters.headers.length === 0) {
                        dispatch('getHeaders', getters.firstUser);
                        commit('DELETE_ID_FROM_HEADERS')
                    }
                    commit('SET_TOTAL_ITEMS');
                })
                .catch(err => {
                    console.log(err.response.data.message || err.message);
                    commit('HANDLE_ERROR', err)
                })
        },
        getHeaders({commit}, payload) {
            for (let [key] of Object.entries(payload)) {
                commit('ADD_HEADERS', `${key}`)
            }
        },
        getNextUsers({commit}) {
            commit('NEXT_PAGE');
        },
        getPrevUsers({commit}) {
            commit('PREV_PAGE')
        },
        goToFirstPage({commit}) {
            commit('FIRST_PAGE')
        },
        goToLastPage({commit, getters}) {
            commit('LAST_PAGE', getters.getPagesAmount)
        },
        getExactUsers({commit}, payload) {
            commit('PARTICULAR_PAGE', payload)
        },
        sortTable({commit, state}, payload) {
            if (payload === state.sortType) {
                commit('CHANGE_DIRECTION')
            } else if (payload !== state.sortType) {
                commit('CHANGE_SORT_TYPE', payload);
                commit('RESET_DIRECTION')
            }
            commit('SORT_USERS', payload)
        },
        showModal({commit}, payload) {
            commit('SHOW_MODAL');
            commit('GET_USER_INFO', payload)
        },
        searchUsers({commit, state}) {
            commit('SEARCH_USERS');
            if (!state.searchList.length) {
                commit('NO_SEARCH_RESULTS')
            }
        }
    },
    getters: {
        users(state) {
            return state.users
        },
        searchList(state) {
            return state.searchList.length
        },
        headers(state) {
            return state.headers
        },
        loading(state) {
            return state.loading
        },
        firstUser(state) {
            return state.users[0]
        },
        getPagesAmount(state) {
            return Math.ceil(state.pagination.total / state.pagination.rowsPerPage)
        },
        getCurrentPage(state) {
            return state.pagination.page
        },
        getPaginatedUsers(state) {
            let clone;
            if (state.searchList.length) {
                clone = state.searchList.map(el => {
                    return {...el}
                })
            } else {
                clone = state.users.map(el => {
                    return {...el}
                })
            }
            const startFrom = (state.pagination.page * state.pagination.rowsPerPage) - state.pagination.rowsPerPage;
            return clone.splice(startFrom, state.pagination.rowsPerPage)
        },
        getModalState(state) {
            return state.showModal
        },
        getSelectedUserInfo(state) {
            return state.selectedUser
        },
        getSortDirection(state) {
            return state.sortDirection
        },
        noSearchResults(state) {
            return state.noUsers
        },
        getError(state) {
            return state.error
        }
    },
    modules: {}
})
